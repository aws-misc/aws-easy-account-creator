if test ! -d .venv; then
    echo "Error: .venv doesn't exist. Run 00_py_init.sh"
    exit 1
fi

source .venv/bin/activate

python main.py $1