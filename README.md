# AWS Easy Account Tool

This tool will easily lazy create and initialize child AWS accounts. It can be considered to be idempotent.


# Prerequisites

	1. Python 3.7 or 3.6
	1. Create a User with admin priviliges and programatic access
	2. Set the credentials in your .aws credentials file under a profile
	3. set up a profile in your .aws config file. set default region here
	3. Need to do prequsities in setting up Organization OU, Microsoft AD and manual steps for SSO 


## example .aws files

.credentials
here `[org_mgr]` is the profile name
```
[org_mgr]
aws_access_key_id=********
aws_secret_access_key=***************
```

.config
```
[profile org_mgr]
region=us-east-1
```

# Instructions

see https://w.amazon.com/bin/view/Setting_up_AWS_Child_Accounts_for_Workshops_&_Immersion_Days/

# How to run

run `00_py_init.sh` to setup the virtual env and install dependencies

```
# Only needs to be run once.
./00_py_init.sh

# This is the main command
./01_run.sh lazy_create_accounts


```

## Sample Output

Account creation is async. so if creating new accounts we have to wait a while for accounts to be created.
```
⇒  ./01_run.sh lazy_create_accounts
Lazy Creating Accounts...
Workshop01 exists
Workshop02 exists
Workshop03 exists
Workshop04 exists
Workshop05 exists
Workshop06 exists
Workshop07 exists
Workshop08 exists
Workshop09 exists
Workshop10 exists
creating Workshop11...
creating Workshop12...
creating Workshop13...
creating Workshop14...
creating Workshop15...
creating Workshop16...
creating Workshop17...
creating Workshop17...
creating Workshop18...
creating Workshop19...
creating Workshop20...
Wait a while for accounts to be created.
```

there is also a max on allowed concurrent account creation requests. so if you get
```
----- ERROR ----
An error occurred (TooManyRequestsException) when calling the CreateAccount operation (reached max retries: 4): AWS Organizations can't complete your request because another request is already in progress. Try again later.
-----
```
simply wait a while and rerun the command

After we have waited for accounts to be created, we simply run command again and get
```
⇒  ./01_run.sh lazy_create_accounts
Lazy Creating Accounts...
Moving account Workshop19 from root to OU
Moving account Workshop20 from root to OU
Moving account Workshop17 from root to OU
Moving account Workshop18 from root to OU
Workshop01 exists
Workshop02 exists
Workshop03 exists
Workshop04 exists
Workshop05 exists
Workshop06 exists
Workshop07 exists
Workshop08 exists
Workshop09 exists
Workshop10 exists
Workshop11 exists
Workshop12 exists
Workshop13 exists
Workshop14 exists
Workshop15 exists
Workshop16 exists
Workshop17 exists
Workshop18 exists
Workshop19 exists
Workshop20 exists
Lazy Setting Accounts Aliases...
Workshop01 - setting alias wlab-wsu01
Workshop02 - setting alias wlab-wsu02
Workshop03 - setting alias wlab-wsu03
Workshop04 - setting alias wlab-wsu04
Workshop05 - setting alias wlab-wsu05
Workshop06 - setting alias wlab-wsu06
Workshop07 - setting alias wlab-wsu07
Workshop08 - setting alias wlab-wsu08
Workshop09 - setting alias wlab-wsu09
Workshop10 - setting alias wlab-wsu10
Workshop11 - setting alias wlab-wsu11
Workshop12 - setting alias wlab-wsu12
Workshop13 - setting alias wlab-wsu13
Workshop14 - setting alias wlab-wsu14
Workshop15 - setting alias wlab-wsu15
Workshop16 - setting alias wlab-wsu16
Workshop17 - setting alias wlab-wsu17
Workshop18 - setting alias wlab-wsu18
Workshop19 - setting alias wlab-wsu19
Workshop20 - setting alias wlab-wsu20
Lazy Creating Workmail Users...
wsu01 exists.
	resetting password...
wsu02 exists.
	resetting password...
wsu03 exists.
	resetting password...
wsu04 exists.
	resetting password...
wsu05 exists.
	resetting password...
wsu06 exists.
	resetting password...
wsu07 exists.
	resetting password...
wsu08 exists.
	resetting password...
wsu09 exists.
	resetting password...
wsu10 exists.
	resetting password...
wsu11 exists.
	resetting password...
wsu12 exists.
	resetting password...
wsu13 exists.
	resetting password...
wsu14 exists.
	resetting password...
wsu15 exists.
	resetting password...
wsu16 exists.
	resetting password...
wsu17 exists.
	resetting password...
wsu18 exists.
	resetting password...
wsu19 exists.
	resetting password...
wsu20 exists.
	resetting password...

```

# FAQ

## Why do we bother setting Account Alias
This is used by aws-nuke

## Why doesn't the tool map my workmail users to accounts?
AWS SSO has no API

https://aws.amazon.com/single-sign-on/faqs/
```
Is there an API available for AWS SSO?

No. You can use the AWS SSO console to perform all necessary operations.
```

## How to get root credentials?
See "Using the root user credentials". https://aws.amazon.com/premiumsupport/knowledge-center/organizations-member-account-access/

## How to delete an account

	1. Get Root credentials

	2. Log in to account

	3. Upper right hand corner under username select my Account

	4. Scroll down to the bottom to "Close Account", check box and click "Close Account"

## How to get the Organization Root id

	1. Log into console

	2. Navigate to AWS Organizations

	3. Go to Organize Accounts

	4. Look at the ARN for Root. The id is the part starting with "r" after the "/" e.g. `arn:aws:organizations::113845159687:root/o-pepnz33vjv/r-8oln` the root id is `r-8oln` 

## I get `EntityAlreadyExists` when setting account alias
Looks like account aliases need to be globally unique. hooray.